import java.util.Random;
public enum Suit {
    SPADE("\u0006"),DIAMOND("\u0004"),
    HEART("\u0003"),CLUB("\u0005"),DEFAULT("");

    private String suit;
    
    private Suit(String suit){
        this.suit = suit;
    }
    
    public static Suit randomSuit(){
        Random ran = new Random();
        int x = ran.nextInt(4)+1;
        Suit ranSuit = DEFAULT;

        switch(x){
            case 1:
                ranSuit = SPADE;
                break;
            case 2:
                ranSuit = DIAMOND;
                break;
            case 3:
                ranSuit = HEART;
                break;
            case 4:
                ranSuit = CLUB;
                break;
        }
        
        return ranSuit;
    }
    public boolean equals (Suit obj){
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }

        if(this != obj){
            return false;
        }
        return true;
    }
    @Override
    public String toString(){
        return this.suit;
    }
}
