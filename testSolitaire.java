
import java.util.Scanner;

public class testSolitaire{
	private final static Scanner in = new Scanner(System.in);
	private static Deck deck = new Deck();
	private static Tableau tableau = new Tableau(deck);
	private static DrawPile drawPile = new DrawPile(deck.getDeck());
	private static Foundation foundation = new Foundation();
	private static int turns = 0;
	
	//private static Foundation foundation = new Foundation();
	public static void main(String[] args) {
		System.out.println("\033[H\033[2J");
		System.out.flush();
		char restart = ' ';
		String test = "r 7";
		int rowNum = -1;
		for(int i = 0; i < test.length(); i++){
			if(Character.isDigit(test.charAt(i))){
				rowNum = test.charAt(i)-48;
			}
		}
		System.out.println(rowNum);


		do{
			System.out.println("\033[H\033[2J");
			System.out.flush();
			playGame();
			System.out.println("Play again? (yes/no)");
			turns = 0;
			String input = in.nextLine();
			input = input.toLowerCase();
			input = input.trim();
			restart = input.charAt(0);
			restart();
		}while(restart != 'n');
	}

	private static void playGame(){
		
		//-----------------------------------
		// Card kingOfHearts = new Card(Suit.HEART, Rank.KING);
		// Card kingOfDiamonnds = new Card(Suit.DIAMOND, Rank.KING);
		// Card kingOfSpades = new Card(Suit.SPADE, Rank.KING);
		// Card kingOfClubs = new Card(Suit.CLUB, Rank.KING);
		// foundation.add(kingOfHearts);
		// foundation.add(kingOfDiamonnds);
		// foundation.add(kingOfClubs);
		// foundation.add(kingOfSpades);
		//-----------------------------------
		System.out.println("██╗    ██╗███████╗██╗      ██████╗ ██████╗ ███╗   ███╗███████╗    ████████╗ ██████╗     ███████╗ ██████╗ ██╗     ██╗████████╗ █████╗ ██╗██████╗ ███████╗\n██║    ██║██╔════╝██║     ██╔════╝██╔═══██╗████╗ ████║██╔════╝    ╚══██╔══╝██╔═══██╗    ██╔════╝██╔═══██╗██║     ██║╚══██╔══╝██╔══██╗██║██╔══██╗██╔════╝\n██║ █╗ ██║█████╗  ██║     ██║     ██║   ██║██╔████╔██║█████╗         ██║   ██║   ██║    ███████╗██║   ██║██║     ██║   ██║   ███████║██║██████╔╝█████╗ \n██║███╗██║██╔══╝  ██║     ██║     ██║   ██║██║╚██╔╝██║██╔══╝         ██║   ██║   ██║    ╚════██║██║   ██║██║     ██║   ██║   ██╔══██║██║██╔══██╗██╔══╝ \n╚███╔███╔╝███████╗███████╗╚██████╗╚██████╔╝██║ ╚═╝ ██║███████╗       ██║   ╚██████╔╝    ███████║╚██████╔╝███████╗██║   ██║   ██║  ██║██║██║  ██║███████╗\n ╚══╝╚══╝ ╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝       ╚═╝    ╚═════╝     ╚══════╝ ╚═════╝ ╚══════╝╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝╚══════╝ \n																																					");
		System.out.println();
		System.out.println("Type 'rules' to see the rules of the game");
		String input = in.nextLine();
		input = input.toLowerCase();
		input = input.trim();
		if(input.equals("rules")){
			System.out.println("	The game consists of a draw pile, foundation and the tableau.");
			System.out.println("	The tableau has 7 rows with cards in them. In the beginning, only the first card in each row will be shown. As cards are removed the next card is shown.");
			System.out.println("	28 cards out of 52 are in the tableau. The remaining 24 are in the draw pile");
			System.out.println("	Cards can be moved to and around the tableau, but you can only move a card onto one that is of opposite colour and a rank above it");
			System.out.println("	Only one card in the draw pile is shown at a time and only the displayed card can be moved. Typing 'next' will show the next card in the draw pile");
			System.out.println("	The goal of the game is to move cards around and fill the 4 foundation piles from ace to king. At which point the game will end");
			System.out.println("                                                                                          ");
		}
		System.out.println("███████╗████████╗ █████╗ ██████╗ ████████╗     ██████╗  █████╗ ███╗   ███╗███████╗██████╗ \n██╔════╝╚══██╔══╝██╔══██╗██╔══██╗╚══██╔══╝    ██╔════╝ ██╔══██╗████╗ ████║██╔════╝╚════██╗\n███████╗   ██║   ███████║██████╔╝   ██║       ██║  ███╗███████║██╔████╔██║█████╗    ▄███╔╝\n╚════██║   ██║   ██╔══██║██╔══██╗   ██║       ██║   ██║██╔══██║██║╚██╔╝██║██╔══╝    ▀▀══╝ \n███████║   ██║   ██║  ██║██║  ██║   ██║       ╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗  ██╗   \n╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝        ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝  ╚═╝   ");
		System.out.println("(yes/no)");
		String userOption = in.nextLine();
		userOption = userOption.trim();
		userOption = userOption.toLowerCase();
		if(!userOption.equals("yes")){
			System.out.println(Colours.RED + "How boring" + Colours.RESET);
			return;
		}
		char userChoice = 'n';
		boolean validAction = false;
		//get the users next move
		do{
			System.out.println(Colours.RESET);
			printDrawPile();
			printFoundation();
			printTableau();
			do{
				System.out.println();
				System.out.println("Type a card (rank then suit seperated by a comma) to move or 'next' to go to the next card in the draw pile");
				String userAction = in.nextLine();
				userAction = userAction.trim();
				userAction = userAction.toLowerCase();
				if(userAction.equals("next")){
					drawPile.nextCard();
					validAction = true;
				}
				else if(userAction.contains(",")){
					String[] userCard = userAction.split(",");
					Card cardToMove = userCardInput(userCard);

					if(cardToMove == null){
						System.out.println(Colours.RED.toString() + "invalid input, input the rank then suit seperated by a comma" + Colours.RESET.toString());
						try{
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						continue;
					}
					if(!tableau.TableauValidCardMove(cardToMove)){
						System.out.println(Colours.RED.toString() + "invalid input, can only move a visible card" + Colours.RESET.toString());
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						continue;
					}
					
					moveACard(cardToMove);
					validAction = true;
				}
				else{
					System.out.println(Colours.RED.toString() + "invalid input, input a card or input next" + Colours.RESET.toString());
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					continue;
				}
				turns++;
			}while(!validAction);

			if(foundation.full()){
				System.out.println("██╗   ██╗ ██████╗ ██╗   ██╗    ██╗    ██╗██╗███╗   ██╗██╗\n╚██╗ ██╔╝██╔═══██╗██║   ██║    ██║    ██║██║████╗  ██║██║ \n ╚████╔╝ ██║   ██║██║   ██║    ██║ █╗ ██║██║██╔██╗ ██║██║ \n  ╚██╔╝  ██║   ██║██║   ██║    ██║███╗██║██║██║╚██╗██║╚═╝\n   ██║   ╚██████╔╝╚██████╔╝    ╚███╔███╔╝██║██║ ╚████║██╗\n   ╚═╝    ╚═════╝  ╚═════╝      ╚══╝╚══╝ ╚═╝╚═╝  ╚═══╝╚═╝");
				return;
			}
			if(turns == 30 || turns == 40 || turns >= 50){
				System.out.println("End game?");
				userOption = in.nextLine();
				userOption = userOption.toLowerCase();
				userChoice = userOption.charAt(0);
			}
			System.out.println("\033[H\033[2J");
			System.out.flush();
		}while(userChoice!='y');

		System.out.println(Colours.RED);
		System.out.println(" ██████╗  █████╗ ███╗   ███╗███████╗     ██████╗ ██╗   ██╗███████╗██████╗ \n██╔════╝ ██╔══██╗████╗ ████║██╔════╝    ██╔═══██╗██║   ██║██╔════╝██╔══██╗\n██║  ███╗███████║██╔████╔██║█████╗      ██║   ██║██║   ██║█████╗  ██████╔╝\n██║   ██║██╔══██║██║╚██╔╝██║██╔══╝      ██║   ██║╚██╗ ██╔╝██╔══╝  ██╔══██╗\n╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗    ╚██████╔╝ ╚████╔╝ ███████╗██║  ██║\n ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝     ╚═════╝   ╚═══╝  ╚══════╝╚═╝  ╚═╝\n																		 ");
		System.out.println(Colours.RESET);

	}

	private static void restart(){
		deck = new Deck();
		tableau = new Tableau(deck);
		drawPile = new DrawPile(deck.getDeck());
		foundation = new Foundation();
	}
	/* Method called every time the user asks to move a card, validates and executes the move
	 * @param userPlay is where the user wants to move the card, a column or foundation
	 * @param userCard calls the userCardInput method and gets what card the user selects
	 */
	private static void moveACard(Card userCard){
		String userPlay = "";
		userCard.show();
		
		System.out.printf("Where do you want to move the %s? type f for foundation, row followed by the row number (ex:r2 or row2), or type 'undo' to cancel move \n", userCard);
		userPlay = in.nextLine();
		userPlay.trim();
		userPlay = userPlay.toLowerCase();
		if(userPlay.equals("undo")){
			return;
		}

		if(userPlay.contains("row") || userPlay.contains("r")){
			int  rowNum = getNumFromPlay(userPlay);
			while(rowNum<1 || rowNum>7){	
				System.out.println(Colours.RED + "The row number must be between 1 and 7" + Colours.RESET);
				userPlay = in.nextLine();
				userPlay.trim();
				userPlay = userPlay.toLowerCase();
				rowNum = getNumFromPlay(userPlay);
			}
			moveOneToColumn(userCard,rowNum);
		}
		else if(userPlay.equals("foundation") || userPlay.contains("f")){
			moveToFoundation(userCard);
		}
	}
	
	private static int getNumFromPlay(String userPlay){
		int rowNum = -1;
		for(int i =0; i < userPlay.length();i++){
			if(Character.isDigit(userPlay.charAt(i))){
				rowNum = userPlay.charAt(i)-48;
			}
		}
		return rowNum;
	}

	/* Method called when the user only moves a single card
	 * @param userColumn is the column the user wants to move a card to
	 */
	private static void moveOneToColumn(Card userCard, int rowNum){
		boolean okay = false;
		//checks if the card they want to move is in the draw pile, if it is it removes it from there before adding it to the column
		if(drawPile.contains(userCard)){
			do{
				try{

					tableau.moveCardFromDrawPile(userCard,rowNum);
					drawPile.removeFromPile(userCard);
					okay = true;
				}catch(IllegalArgumentException iae){
					System.out.println(Colours.RED + iae.getMessage() + Colours.RESET);
				}catch(NullPointerException NPE){
					System.out.println(Colours.RED + NPE.getMessage() + Colours.RESET);
				}catch(IndexOutOfBoundsException IOBE){
					System.out.println(Colours.RED + IOBE.getMessage() + Colours.RESET);
				}
			}while(!okay);	
		}
		else{
			do{
				try{
					tableau.moveCardFromTableau(userCard, rowNum);
					okay = true;
				} catch(IllegalArgumentException iae){
					System.out.println(Colours.RED + iae.getMessage() + Colours.RESET);
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					return;
				} catch(NullPointerException NPE){
					System.out.println(Colours.RED + NPE.getMessage() + Colours.RESET);
				} catch(IndexOutOfBoundsException IOBE){
					System.out.println(Colours.RED + IOBE.getMessage() + Colours.RESET);
				}
				
			}while(!okay);
		}
	}

	private static Card userCardInput(String[] userCard){
		String userSuit = "";
		String userRank = "";
		Rank cardRank = Rank.DEFAULT;
		Suit cardSuit = Suit.DEFAULT;
		Card returnCard = new Card();

		userRank = userCard[0];
		userRank = userRank.toLowerCase();
		userRank = userRank.trim();
		userSuit = userCard[1];
		userSuit = userSuit.toLowerCase();
		userSuit = userSuit.trim();
		
		switch(userSuit){
			case "diamond": case "diamonds":
				cardSuit = Suit.DIAMOND;
				break;
			case "spade": case "spades":
				cardSuit = Suit.SPADE;
				break;
			case "heart": case "hearts":
				cardSuit = Suit.HEART;
				break;
			case "club": case "clubs":
				cardSuit = Suit.CLUB;
				break;
			default:
				cardSuit = Suit.DEFAULT;
		}
		switch(userRank){
            case "ace": case "1": case "one": case "a":
				cardRank = Rank.ACE;
				break;
            case "2" : case "two":
				cardRank = Rank.TWO;
				break;
            case "3": case "three":
            	cardRank = Rank.THREE;
                break;
            case "4": case "four":
				cardRank = Rank.FOUR;
				break;
            case "5": case "five": 
				cardRank = Rank.FIVE;
				break;
            case "6": case "six": 
				cardRank = Rank.SIX;
				break;
            case "7": case "seven": 
				cardRank = Rank.SEVEN;
				break;
            case "8": case "eight": 
				cardRank = Rank.EIGHT;
				break;
            case "9": case "nine": 
				cardRank = Rank.NINE;
				break;
            case "10": case "ten": 
				cardRank = Rank.TEN;
				break;
            case "11": case "eleven": case "jack": case "j" :
				cardRank = Rank.JACK;
				break;
            case "12": case "twelve": case "queen": case "q" :
				cardRank = Rank.QUEEN;
				break;
            case "13": case "thirteen": case "king": case "k" :
				cardRank = Rank.KING;
				break;
			default: 
				cardRank = Rank.DEFAULT;
				break;
        }
		if(cardSuit.equals(Suit.DEFAULT) || cardRank.equals(Rank.DEFAULT)){
			returnCard = null;
		}
		else{
			returnCard = new Card(cardSuit, cardRank);
		}
		if(drawPile.contains(returnCard)){
			try{
				drawPile.validCardMove(returnCard);
			}catch (IllegalArgumentException IAE) {
				System.out.println(Colours.RED + IAE.getMessage() + Colours.RESET);
				return null;
			}
		}
		return returnCard;
	}

	//this method makes sure the users input is seperated by a ','
	// private static boolean validateInput(String userIn){
	// 	boolean validCard = false;
	// 	while(!(validCard)){
	// 		if(!(userIn.contains(","))){
	// 			System.out.println(Colours.RED.toString() + "Not a valid card" + Colours.RESET.toString() +
	// 			 " please insert the suit (diamond, heart etc.) then the rank (ace,two,king etc) seperated by a comma");
	// 			userIn = in.nextLine();
	// 			validCard = false;
	// 			continue;
	// 		}
	// 		validCard = true;
	// 	}//end valid card while loop
	// 	return validCard;

	// }

	//moves card to the foundation, removes it from the approriate departure location
	private static void moveToFoundation(Card userCard){
		try{
			if(drawPile.contains(userCard)){
				foundation.add(userCard);
				drawPile.removeFromPile(userCard);
			}
			else{
				foundation.add(userCard);
				tableau.removeElement(userCard);
			}
		}catch(IllegalArgumentException IAE){
			System.out.println(Colours.RED + IAE.getMessage() + Colours.RESET);
			System.out.println("ugh");
			return;
		}
	}

	private static void printTableau(){
		System.out.println("Tableau");
		System.out.println(tableau);
		System.out.println();
	}

	private static void printDrawPile(){
		System.out.println("Draw Pile");
		System.out.println(drawPile);
	}
	private static void printFoundation(){
		System.out.println("Foundation");
		System.out.println(foundation);
		System.out.println();
	}
}