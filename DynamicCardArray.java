public class DynamicCardArray{
    private Card[] data;
    private int pointer;
    
    public DynamicCardArray(){
      this.data = new Card[100];
      this.pointer = 0;
    }

    public boolean isEmpty(){
        if(this.data[0] == null){
            return true;
        }
        return false;
    }

    //adding an element
    public void add(Card s){
        data[pointer] = s;
        pointer++;
    }

    public int length(){
        return this.pointer;
    }
    public Card getLast(){
        Card blank = new Card();
        if(pointer == 0 ){
            if(this.data[0] == null){
                return blank;
            }
            return this.data[0];
        }
        if(this.data[pointer-1] == null){
            return blank;
        }
        return this.data[pointer-1];
    }

    public boolean hasChild(Card parentCard){
        int indexOfParentCard = getIndexOf(parentCard);
        if(this.data[indexOfParentCard+1] != null){
            return true;
        }
            return false;
    }

    public void removeElement(int index){
        this.data[index]=null;
        for(int i=index+1;i<pointer;i++){
          this.data[i-1]=data[i];
        }
        pointer--;
      }

    //check if a card is present in the array
    public boolean contains(Card check){
        return getIndexOf(check) != -1;
    }

    //returning a specific card
    public Card get(int index){ 
        if(this.data[index] == null){
            Card blank = new Card();
            return blank; 
        }
        return this.data[index];
    }

    //returns the index of a card
    public int getIndexOf(Card search){
        for(int i = 0; i < this.pointer; i++){
            if(this.data[i] == null){
                continue;
            }
            if (this.data[i].equals(search)){
                return i;
            }
        }
        return -1;
    }

    //returning all the data in the array
    public Card[] getData(){
        return this.data;
    }

    //changing a card in the array 
    public void changeCard(int index, Card change){
        if(change == null){
            this.data[index] = change;
            pointer--;
        }
        else{
            this.data[index] = change;
        }
    }

    public DynamicCardArray getSubArray(Card firstCard){
        int first = this.getIndexOf(firstCard);
        DynamicCardArray subArray = new DynamicCardArray();
        for(int i = first; i < this.length(); i++){
            subArray.add(this.data[i]);
        }
        return subArray;

    }

    public void order(){
    
    }

    @Override
    public String toString(){
        String s = "";
        for (int i = 0; i<this.pointer; i++){
                if(this.data[i]==null){
                    s += "null, ";
                    continue;
                }
                s += this.data[i] + "";
                s +=", ";
        }
        return s;
    }
}
