public class Foundation extends DynamicCardArray{
    private DynamicCardArray HeartColumn = new DynamicCardArray();
    private DynamicCardArray DiamondColumn = new DynamicCardArray();
    private DynamicCardArray SpadeColumn = new DynamicCardArray();
    private DynamicCardArray ClubColumn = new DynamicCardArray();


    public void add(Card inputCard){
        Suit inputSuit = inputCard.getSuit();
        DynamicCardArray columnToChange = new DynamicCardArray();
        switch(inputSuit){
			case DIAMOND :
				columnToChange = DiamondColumn;
				break;
			case SPADE :
				columnToChange = SpadeColumn;
				break;
			case HEART :
                columnToChange = HeartColumn;
				break;
			case CLUB :
                columnToChange = ClubColumn;
				break;
            default :
                throw new IllegalArgumentException("Card cant be added");
		}
        if(columnToChange.isEmpty()){
            if(inputCard.getRank().equals(Rank.ACE)){
                columnToChange.add(inputCard);
            }
            else{
                throw new IllegalArgumentException("Foundation piles can only start with an ace");
            }
        }
        else{
                Card lastCard = columnToChange.getLast();
                    int inputCardValue = inputCard.getRankValue();
                    int lastCardValue = lastCard.getRankValue();
                    if(inputCardValue == (lastCardValue+1)){
                        columnToChange.add(inputCard);
                    }
                    else{
                         throw new IllegalArgumentException("Cards can only be added to the foundation in order of their rank");
                    }
        }
        
        switch(inputSuit){
			case DIAMOND :
				DiamondColumn = columnToChange;
				break;
			case SPADE :
				SpadeColumn = columnToChange;
				break;
			case HEART :
                HeartColumn = columnToChange;
				break;
			case CLUB :
                ClubColumn = columnToChange;
				break;
            default :
                throw new IllegalArgumentException("Card cant be added");
		}
        
    }
    public boolean empty(){
        if(HeartColumn.isEmpty() && DiamondColumn.isEmpty() && SpadeColumn.isEmpty() && ClubColumn.isEmpty()){
            return true;
        }
        return false;
    }

    public boolean full(){
        boolean heartFull = false;
        boolean diamondFull = false;
        boolean spadeFull = false;
        boolean clubFull = false;
        if(HeartColumn.length() == 0){
            heartFull = false;
        }
        else if(HeartColumn.get(HeartColumn.length()-1).getRank().equals(Rank.KING)){
            heartFull = true;
        }

        if(DiamondColumn.length() == 0){
            diamondFull = false;
        }
        else if(DiamondColumn.get(DiamondColumn.length()-1).getRank().equals(Rank.KING)){
            diamondFull = true;
        }

        if(SpadeColumn.length() == 0){
            spadeFull = false;
        }
        else if(SpadeColumn.get(SpadeColumn.length()-1).getRank().equals(Rank.KING)){
            spadeFull = true;
        }

        if(ClubColumn.length() == 0){
            clubFull = false;
        }
        else if(ClubColumn.get(ClubColumn.length()-1).getRank().equals(Rank.KING)){
            clubFull = true;
        }

        if(heartFull && diamondFull && spadeFull && clubFull){
            return true;
        }

        return false;
    }

    @Override
    public String toString(){
        String out = "";
        DiamondColumn.getLast().show();
        SpadeColumn.getLast().show();
        HeartColumn.getLast().show();
        ClubColumn.getLast().show();
        String columnNums =
        String.format(" %-5.5s %-5.5s %-5.5s %-5.5s",
        "1", "2", "3", "4");
        String columns = "";
        out += columnNums;
        out+="\n";
        if(empty()){
            columns = 
            String.format(" %-5.5s  %-5.5s %-5.5s %-5.5s",
            "  ", "  ", "  ", "  ");
        }
        else{
            columns +=
            DiamondColumn.getLast() + " " + SpadeColumn.getLast() + "  " + HeartColumn.getLast() + "  " + ClubColumn.getLast();
        }
        out += columns;
        return out;
    }
}

