public class Tableau extends DynamicCardArray{
    private DynamicCardArray column1 = new DynamicCardArray();
    private DynamicCardArray column2 = new DynamicCardArray();
    private DynamicCardArray column3 = new DynamicCardArray();
    private DynamicCardArray column4 = new DynamicCardArray();
    private DynamicCardArray column5 = new DynamicCardArray();
    private DynamicCardArray column6 = new DynamicCardArray();
    private DynamicCardArray column7 = new DynamicCardArray();

    public Tableau(Deck deck){ 
        Card newCard = new Card();
        for(int i = 0; i <= 27; i++){
            newCard = Card.ranCard();
                if(deck.contains(newCard)){
                        if(i==0){
                            this.column1.add(newCard);
                        }
                        if(i==1 || i==2){
                            this.column2.add(newCard);
                            this.column2.get(i).hide();
                        }
                        if(i>=3 && i<=5){
                            this.column3.add(newCard);
                            this.column3.get(i).hide();
                        }
                        if(i>=6 && i<=9){
                            this.column4.add(newCard);
                            this.column4.get(i).hide();
                        }
                        if(i>=10 && i<=14){
                            this.column5.add(newCard);
                            this.column5.get(i).hide();
                        }
                        if(i>=15 && i<=20){
                            this.column6.add(newCard);
                            this.column6.get(i).hide();
                        }
                        if(i>=21 && i<=27){
                            this.column7.add(newCard);
                            this.column7.get(i).hide();
                        }
                        deck.removeCard(newCard);
                }
                else{
                    i-=1;
                }
        }
        updateTableau(column1);
        updateTableau(column2);
        updateTableau(column3);
        updateTableau(column4);
        updateTableau(column5);
        updateTableau(column6);
        updateTableau(column7);
    }

    /*
     * @param columnTo is number of the column the card will be moved to (destination)
     * @param moveFromHere is number of the column the card will be moved from (departure)
     * @param moveFromHere is a temporary variable for the departure column
     * @param moveToHere is a temporary variable for the destination column
     */
    public void moveCardFromTableau(Card cardToMove, int columnTo){
        int columnFrom = cardLocationInt(cardToMove);
        DynamicCardArray moveFromHere = cardLocation(cardToMove);
        DynamicCardArray moveToHere = getColumnTo(columnTo);
        DynamicCardArray subArray = new DynamicCardArray();
        if (moveFromHere.equals(moveToHere)){
            throw new IllegalArgumentException("Cannot move to the same column");
        }
        if(columnTo < 1){
            throw new IndexOutOfBoundsException("row number cant be less than 1");
        }

        //update the moveToHere temporary array to the selected column
        Card movedOnto = moveToHere.getLast();
        try{
            cardToMove.placeUnder(movedOnto);
        }catch(IllegalArgumentException IAE){
            throw new IllegalArgumentException(IAE.getMessage());
        }

        //add and remove card from columns
        int indexOfRemoveCard;
        if(moveFromHere.hasChild(cardToMove)){
            subArray = moveFromHere.getSubArray(cardToMove);
            for (Card card : subArray.getData()){
                if(card == null){
                    continue;
                }
                moveToHere.add(card);
                indexOfRemoveCard = moveFromHere.getIndexOf(card);
                moveFromHere.removeElement(indexOfRemoveCard);
            }
        }
        else{
            indexOfRemoveCard = moveFromHere.getIndexOf(cardToMove);
            moveFromHere.removeElement(indexOfRemoveCard);
            moveToHere.add(cardToMove);
        }
        updateTableau(moveFromHere);

        //update the selected departure column to the moveFromHere 
        switch (columnFrom){
            case 1:
                column1 = moveFromHere;
                break;
            case 2:
                column2 = moveFromHere;
                break;
            case 3:
                column3 = moveFromHere;
                break;
            case 4:
                column4 = moveFromHere;
                break;
            case 5:
                column5 = moveFromHere;
                break;
            case 6:
                column6 = moveFromHere;
                break;
            case 7:
                column7 = moveFromHere;
                break;
        }

        //update the selected destination column to the movetoHere
        switch(columnTo){
            case 1:
                column1 = moveToHere;
                break;
            case 2:
                column2 = moveToHere;
                break;
            case 3:
                column3 = moveToHere;
                break;
            case 4:
                column4 = moveToHere;
                break;
            case 5:
                column5 = moveToHere;
                break;
            case 6:
                column6 = moveToHere;
                break;
            case 7:
                column7 = moveToHere;
                break;
        }

    }
    /*
     * Same as above, this one doesnt need to remove a card since it comes from the draw pile
     */
    public void moveCardFromDrawPile(Card cardToMove, int columnTo){
        DynamicCardArray moveToHere = getColumnTo(columnTo);
        if(columnTo < 1 || columnTo > 7){
            throw new IndexOutOfBoundsException("row number cant be less than 1 or bigger than 7");
        }
        //update the moveToHere temporary array to the selected column
        Card movedOnto = moveToHere.getLast();
        if(moveToHere.length() != 0){
            cardToMove.placeUnder(movedOnto);
        }
        moveToHere.add(cardToMove);
        //update the correct column to its new cards
        
        switch(columnTo){
            case 1:
                column1 = moveToHere;
                break;
            case 2:
                column2 = moveToHere;
                break;
            case 3:
                column3 = moveToHere;
                break;
            case 4:
                column4 = moveToHere;
                break;
            case 5:
                column5 = moveToHere;
                break;
            case 6:
                column6 = moveToHere;
                break;
            case 7:
                column7 = moveToHere;
                break;
        }
    }
    //returns the column # of the card
    private int cardLocationInt(Card search){
        int columnNum = -1;
        if(column1.contains(search)){
            columnNum = 1;
        }
        if(column2.contains(search)){
            columnNum = 2;
        }
        if(column3.contains(search)){
            columnNum = 3;
        }
        if(column4.contains(search)){
            columnNum = 4;
        }
        if(column5.contains(search)){
            columnNum = 5;
        }
        if(column6.contains(search)){
            columnNum = 6;
        }
        if(column7.contains(search)){
            columnNum = 7;
        }
        return columnNum;
    }
    //returns the columnof the card
    private DynamicCardArray cardLocation(Card search){
        DynamicCardArray returnColumn = new DynamicCardArray();
        if(column1.contains(search)){
            returnColumn = column1;
        }
        if(column2.contains(search)){
            returnColumn = column2;
        }
        if(column3.contains(search)){
            returnColumn = column3;
        }
        if(column4.contains(search)){
            returnColumn = column4;
        }
        if(column5.contains(search)){
            returnColumn = column5;
        }
        if(column6.contains(search)){
            returnColumn = column6;
        }
        if(column7.contains(search)){
            returnColumn = column7;
        }
        return returnColumn;
    }
    
    public boolean TableauValidCardMove(Card cardMoveToCheck){
        DynamicCardArray columnToCheck = cardLocation(cardMoveToCheck);
        int index = columnToCheck.getIndexOf(cardMoveToCheck);
        if(columnToCheck.get(index).visible()){
            return true;
        }
        return false;
    }

    private DynamicCardArray getColumnTo(int columnTo){
        switch(columnTo){
            case 1:
                return column1;
            case 2:
                return column2;
            case 3:
                return column3;
            case 4:
                return column4;
            case 5:
                return column5;
            case 6:
                return column6;
            case 7:
                return column7;
            default:
                return null;
        }
    }
    private void updateTableau(DynamicCardArray column){
        Card currentCard;
        for(int i =0; i < column.length(); i++){
            currentCard = column.get(i);
            if(column.hasChild(currentCard)){
                column.get(i).hide();
            }
            else{
                column.get(i).show();
            }
        }
    }
    
    public void removeElement(Card cardToRemove){
        DynamicCardArray newColumn = cardLocation(cardToRemove);
        int index = newColumn.getIndexOf(cardToRemove);
        newColumn.removeElement(index);
        updateTableau(newColumn);
        int columnNum = cardLocationInt(cardToRemove);
        switch (columnNum){
            case 1:
                column1 = newColumn;
                break;
            case 2:
                column2 = newColumn;
                break;
            case 3:
                column3 = newColumn;
                break;
            case 4:
                column4 = newColumn;
                break;
            case 5:
                column5 = newColumn;
                break;
            case 6:
                column6 = newColumn;
                break;
            case 7:
                column7 = newColumn;
                break;
        }
    }

    @Override
    public String toString(){    
        String returnTableau = "";
        returnTableau += "1: " + column1 + "\n";
        returnTableau += "2: " + column2 + "\n";
        returnTableau += "3: " + column3 + "\n";
        returnTableau += "4: " + column4 + "\n";
        returnTableau += "5: " + column5 + "\n";
        returnTableau += "6: " + column6 + "\n";
        returnTableau += "7: " + column7 + "\n";

        return returnTableau;
    }

    public String testString(){
        String returnTableau ="";
        returnTableau +=
        String.format("%-5.5s", column1);


        return returnTableau;
    }

}